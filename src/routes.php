<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/osoby', function (Request $request, Response $response, $args) {
    $q = $request->getQueryParam('q');

    try {
        if (empty($q)) {
            $stmt = $this->db->prepare(' SELECT * FROM person ORDER BY last_name');
        } else {
            $stmt = $this->db->prepare(' SELECT * FROM person WHERE last_name ILIKE :q OR first_name ILIKE :q ORDER BY last_name');
            $stmt->bindValue(':q', $q . '%');
        }
        $stmt->execute();
    } catch (Exception $ex) {

        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['osoby'] = $stmt->fetchAll();
    $tplVars['q'] = $q;
    return $this->view->render($response, "osoby.latte", $tplVars);

})->setName("osoby");

$app->get('/add-person', function (Request $request, Response $response, $args) {
    $tplVars['form'] = ['ln' => '', 'nn' => '', 'fn' => ''];
    return $this->view->render($response, 'add-person.latte', $tplVars);
})->setName('addPerson');


$app->post('/add-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])) {
        try {
            $stmt = $this->db->prepare('INSERT INTO person(gender, last_name, first_name, nickname)VALUES(:g, :ln, :fn, :nn)');

            $g = empty($data['g']) ? null : $data['g'];

            $stmt->bindValue(':g', $g);

            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();
        } CATCH (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tato osoba uz existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'add-person.latte');
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
        return $response->withHeader('Location', $this->router->pathFor('osoby'));
    } else {
        $tplVars['form'] = $data;
        return $this->view->render($response, 'add-person.latte', $tplVars);
    }

});

/*
 *
 *     return $response->withHeader('location', $this->router->pathFor('osoby'));
}

else {
    $tplVars['form'] = $data;
    $tplVars['error'] = 'Neni nic zadany';

        $sql="SELECT * from person";
    $stmt=$this->db->prepare($sql);
    $stmt->execute();
    $tplVars['osoby']=$stmt->fetchAll();




* */